# Лабораторная №4

## Ход работы

## Адрес
г. Москва, Багратионовский проезд д. 7 к 3

## Режим работы
* Ежедневно, круглосуточно
* Бассейн с 7:00 до 23:00

## Контакты
* [Телефон](tel:+74950233339)
* [Сайт](https://fusion-fitness.ru/bagrationovskaya/)
* [Почта](mailto:bagration@fusion-fitness.ru)
* [WhatsApp](https://api.whatsapp.com/send?phone=79995771006)

## Часовой пояс
Europe/Moscow

## Аватарка клуба
![](https://i.imgur.com/YcSOogs.jpg)

## Основная картинка клуба
![](https://i.imgur.com/CeWnWJW.jpg)

